use std::process::Command;

pub struct MesonConfig {
    project_dir: String,
    install_dir: String,
    build_dir: String,
    opt_level: String,
    compiler: Compiler,
    install: bool,
    have_native_file: bool,
}

pub enum Compiler {
    MSVC(&'static str),
    GNU(&'static str),
}

impl Compiler {
    pub fn get_file(&self) -> &'static str {
        match self {
            Compiler::MSVC(c) => c,
            Compiler::GNU(c) => c,
        }
    }
}

impl MesonConfig {
    pub const MSVC: Compiler = Compiler::MSVC("native-msvc.build");
    pub const GNU: Compiler = Compiler::GNU("native-gnu.build");

    pub fn new(project_dir: String, opt_level: String) -> MesonConfig {
        let build_dir = format!("Build\\{}", opt_level);

        let install_dir = format!("{}\\Build\\install", project_dir);

        MesonConfig {
            project_dir,
            build_dir,
            opt_level,
            compiler: MesonConfig::MSVC,
            install_dir,
            install: false,
            have_native_file: false,
        }
    }

    pub fn set_install_dir(mut self, install_dir: String) -> MesonConfig {
        self.install_dir = install_dir;
        self
    }

    pub fn should_install(mut self, install: bool) -> MesonConfig {
        self.install = install;
        self
    }

    pub fn have_native_file(mut self, have_native: bool) -> MesonConfig {
        self.have_native_file = have_native;
        self
    }

    pub fn set_compiler(mut self, compiler: Compiler) -> MesonConfig {
        self.compiler = compiler;
        self
    }

    pub fn build(self) -> Meson {
        Meson(self)
    }
}

pub struct Meson(MesonConfig);

impl Meson {
    pub fn run(&self) {
        //print config
        self.print_config();

        //Run Meson Setup
        self.meson_setup();

        //Run Meson Compile
        self.meson_compile();

        //Run Meson Install
        if self.0.install {
            self.meson_install();
        }
    }

    fn meson_setup(&self) {
        match self.0.have_native_file {
            true => {
                //meson setup --native-file native.build --buildtype release/debugoptimized Build\release/debugoptimized --wipe

                let native = self.0.compiler.get_file();

                let cmd_status = Command::new("meson")
                    .current_dir(self.0.project_dir.as_str())
                    .args(["setup","--prefix", "/", "--native-file", native, "--buildtype", self.0.opt_level.as_str(), self.0.build_dir.as_str(),"--wipe"])
                    .status()
                    .expect("Failed to run meson command. Make sure meson is installed and is in PATH environment variable");

                assert!(cmd_status.success());
            }

            false => {
                //meson setup --native-file native.build --buildtype release/debugoptimized Build\release/debugoptimized --wipe

                let cmd_status = Command::new("meson")
                    .current_dir(self.0.project_dir.as_str())
                    .args(["setup","--prefix", "/", "--buildtype", self.0.opt_level.as_str(), self.0.build_dir.as_str(), "--wipe"])
                    .status()
                    .expect("Failed to run meson command. Make sure meson is installed and is in PATH environment variable");

                assert!(cmd_status.success());
            }
        }
    }

    fn meson_compile(&self) {
        //meson compile -C Build\Release
        let cmd_status = Command::new("meson")
            .current_dir(self.0.project_dir.as_str())
            .args(["compile", "-C", self.0.build_dir.as_str()])
            .status()
            .expect("Failed to run meson command. Make sure meson is installed and is in PATH environment variable");

        assert!(cmd_status.success());
    }

    fn meson_install(&self) {
        //meson install --destdir install_dir -C Build\Release

        let cmd_status = Command::new("meson")
            .current_dir(self.0.project_dir.as_str())
            .args(["install", "--destdir", self.0.install_dir.as_str(), "-C", self.0.build_dir.as_str()])
            .status()
            .expect("Failed to run meson command. Make sure meson is installed and is in PATH environment variable");

        assert!(cmd_status.success());
    }

    fn print_config(&self) {
        println!("\n------------- Meson Config -------------------------");
        println!("Project Directory: {}", self.0.project_dir);
        println!("Install Directory: {:?}", self.0.install_dir);
        println!("Build Directory: {}", self.0.build_dir);
        println!("");
        println!("Optimization Level: {}", self.0.opt_level);
        println!("Using Native Build File: {}", self.0.have_native_file);
        println!("Install Meson Project: {}", self.0.install);
        println!("---------------------------------------------------\n");
    }
}
