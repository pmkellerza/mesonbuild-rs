fn main() {
    let project_dir = std::env::var("CARGO_MANIFEST_DIR").unwrap();

    //get optimized level
    let profile = std::env::var("PROFILE").unwrap();

    let meson_opt_level = match profile.as_str() {
        "debug" => String::from("debugoptimized"),
        "release" => String::from("release"),
        _ => unreachable!(),
    };

    //-------------  Set Build environments ---------------
    // Custom Install Dir
    let custom_install_dir = format!("{}\\{}", project_dir, "target\\install");
    println!(
        "cargo:warning=Custom Install Directory Set: {}",
        custom_install_dir
    );

    //------------------------------------------------------

    // Custom Meson Install Dir
    let custom_install_dir = format!("{}\\{}", out_dir, "install");

    //create meson config
    match build_target.as_str() {
        "x86_64-pc-windows-msvc" => {
            let meson = MesonConfig::new(meson_proj_dir, meson_opt_level)
                .have_native_file(true)
                .should_install(true)
                .set_install_dir(custom_install_dir)
                .set_compiler(MesonConfig::MSVC)
                .build();

            meson.run();
        }
        "x86_64-pc-windows-gnu" => {
            let meson = MesonConfig::new(meson_proj_dir, meson_opt_level)
                .have_native_file(true)
                .should_install(true)
                .set_install_dir(custom_install_dir)
                .set_compiler(MesonConfig::GNU)
                .build();

            meson.run();
        }
        _ => unreachable!(),
    }

    //----------------------   Bindgen Wrapper Bindings ----------------------------
    let ray_bindings = bindgen::Builder::default()
        .header("./deps/raylib_wrapper.h")
        .generate()
        .expect("Could not generate Raylib bindings");

    let path = format!("{}\\install", out_dir);
    let out_path = std::path::PathBuf::from(path);

    ray_bindings
        .write_to_file(out_path.join("ray_bindings.rs"))
        .expect("Could not write bindings");
}
